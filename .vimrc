" ---vim-plug
call plug#begin('~/.vim/plugged')
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'https://github.com/easymotion/vim-easymotion'
Plug 'https://github.com/nathanaelkane/vim-indent-guides'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'ap/vim-css-color'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'gitgutter/Vim'
call plug#end()

colorscheme nord

" ---EasyMotion
let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1

" ---Python Mode
let g:pymode = 1
let g:pymode_trim_whitespaces = 1
let g:pymode_indent = 1
let g:pymode_folding = 0
let g:pymode_lint = 0
let g:pymode_python = 'python3'
let g:pymode_lint_write = 0
let g:pymode_options_colorcolumn = 0
"let g:pymode_options_max_line_length = 79

" ---Indent Guides
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 0
let g:indent_guides_guide_size = 1
hi IndentGuidesOdd ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey

" ---Rainbow Parentheses
au VimEnter * RainbowParentheses
let g:rainbow#blacklist = [000]

" ---Airline
let g:airline_powerline_fonts = 1
let g:airline_theme = 'nord'

" ---NERDTree
let NERDTreeShowHidden = 1


" ---My Settings
set relativenumber
set noshowmode
set number
set laststatus=2
set wildmenu
set hlsearch
set history=100
set showcmd
syntax on
set scrolloff=10
set incsearch
set ignorecase
set tabstop=4 softtabstop=4
set expandtab
set smartindent
set smartcase
set shiftwidth=4
set splitbelow splitright
set nofoldenable
autocmd InsertEnter * norm zz
autocmd BufWritePre * %s/\s\+$//e
let mapleader=" "

" ---Cursorline Settings
set cursorline
set cursorcolumn
"set colorcolumn=79
hi CursorLine ctermbg=238 cterm=bold
hi CursorColumn ctermbg=238
"hi ColorColumn ctermbg=8
hi Visual ctermfg=232 ctermbg=214

" ---Mappings
map <leader>s :%s//gI<Left><Left><Left>
map <leader>w :w!<CR>
map <leader>q :q!<CR>
map <leader>wq :wq!<CR>
map <leader>o :only<CR>
map <leader>rl :edit!<CR>
map <leader>lnt :PymodeLint<CR>
map <leader>pep :PymodeLintAuto<CR>
map <C-n> :NERDTreeToggle<CR>
nmap <C-l> <C-w>l
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
map <leader>vs :vsplit<CR>
map <leader>hs :split<CR>
nmap üü O
nmap ää o
imap jj <ESC>
map <leader>j <Plug>(easymotion-j)
map <leader>k <Plug>(easymotion-k)
nmap s <Plug>(easymotion-overwin-f2)
map <leader>h <Plug>(easymotion-linebackward)
map <leader>l <Plug>(easymotion-lineforward)
