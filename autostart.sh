#!/bin/sh
sxhkd -c ~/.dotfiles/.sxhkdrc &
picom --config ~/.config/picom.conf &
feh --bg-fill /usr/share/pixmaps/uni_wallpaper.jpg
