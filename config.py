from libqtile import qtile
from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from typing import List
import os
import subprocess


# setting some variables

mod = "mod4"
home = os.path.expanduser('~')
myfont = "Ubuntu Mono"

colors = ('#D8DEE9',  # foreground 0
          '#2E3440',  # background 1
          '#3B4252',  # black 1    2
          '#4C566A',  # black 2    3
          '#BF616A',  # red 1      4
          '#BF616A',  # red 2      5
          '#A3BE8C',  # green 1    6
          '#A3BE8C',  # green 2    7
          '#EBCB8B',  # yellow 1   8
          '#EBCB8B',  # yellow 2   9
          '#81A1C1',  # blue 1     10
          '#81A1C1',  # blue 2     11
          '#B48EAD',  # magenta 1  12
          '#B48EAD',  # magenta 2  13
          '#88C0D0',  # cyan 1     14
          '#8FBCBB',  # cyan 2     15
          '#E5E9F0',  # white 1    16
          '#ECEFF4')  # white 2    17

orange = ('#CD6600',  #darkorange  0
          '#FF7F00')  #lightorange 1

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Grow and shrink windows in XMonadTall
    Key([mod], "h", lazy.layout.shrink(),
                    lazy.layout.decrease_nmaster()),
    Key([mod], "l", lazy.layout.grow(),
                    lazy.layout.increase_nmaster()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "Tab", lazy.layout.next()),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout()),
    Key([mod], "q", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "s", lazy.shutdown()),
    Key([mod, "mod1"], "Return", lazy.spawncmd()),
    # Key([mod], "Escape", lazy.spawn("sh /home/ronny/.scripts/dmenu_shutdown_menu.sh")),

    Key([mod, "control"], "1", lazy.to_screen(0)),
    Key([mod, "control"], "2", lazy.to_screen(1)),

]

groups = [Group(i) for i in "1234"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

layout_defaults = dict(
    border_width=2,
    border_focus=orange[1],
    border_normal=colors[2],
    margin=15
	)

layouts = [
    layout.MonadTall(**layout_defaults),
    layout.Max(**layout_defaults),
    ]

widget_defaults = dict(
    font=myfont,
    fontsize=18,
    padding=5,
    foreground=colors[0]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.GroupBox(background=orange[1], active=colors[1], inactive=colors[0], padding=3),
                widget.Prompt(),
                widget.WindowName(background=orange[0], foreground=colors[1]),
                widget.Systray(background=orange[0]),
                widget.Clipboard(background=orange[1], foreground=colors[1],timeout=None),
                widget.Volume(background=orange[0], foreground=colors[1]),
                widget.CurrentLayout(background=orange[1], foreground=colors[1]),
                widget.CPU(background=orange[0], foreground=colors[1], format='{freq_current}GHz {load_percent}%'),
                widget.Memory(background=orange[1], foreground=colors[1]),
                widget.Net(background=orange[0], foreground=colors[1], format='{down} ↓↑ {up}'),
                widget.Clock(format='%H:%M %a %d.%m.%Y', background=orange[1], foreground=colors[1]),
            ],
            24, background=colors[1]
        ),
    ),
    Screen(
        bottom=bar.Bar(
            [
                widget.GroupBox(background=orange[1], active=colors[1], inactive=colors[0], padding=3),
                widget.Prompt(),
                widget.WindowName(background=orange[0], foreground=colors[1]),
                widget.Volume(background=orange[1], foreground=colors[1]),
                widget.CurrentLayout(background=orange[0], foreground=colors[1]),
            ],
            24, background=colors[1]
        )
    )
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='show-file-dialog-gtk3'),
    Match(wm_class='pavucontrol'),
    Match(wm_class='gcolor2'),
    Match(wm_class='arandr'),
    Match(wm_class='Steam'),
    Match(wm_class='VirtualBox Manager'),
    Match(wm_class='VirtualBox Machine'),
    Match(wm_class='thunar'),
    Match(wm_class='kite'),
    Match(wm_class='GameConqueror.py'),
    Match(wm_class='hotkeys.py'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# Autostart #

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# Autostart End #


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
